const GameState = Object.freeze({
    WELCOMING:   Symbol("welcoming"),
    WINDOW:  Symbol("window"),
    INVESTIGATE: Symbol("investigate"),
    FIREPLACE: Symbol("fireplace"),
    TAP: Symbol("tap"),
    OUTSIDE: Symbol("outside"),
    BUSH: Symbol("bush")
});

export default class Game{
    constructor(){
        this.stateCur = GameState.WELCOMING;
    }
    
    makeAMove(sInput)
    {
        let sReply = "";
        switch(this.stateCur){
            case GameState.WELCOMING:
                sReply = "It is a dark and rainy night. You are lounging out by the fireplace reading a book, when suddenly BANG! The back door swings open! You go to close it, do you investigate outside and see what caused it or do you close the door and go back to the fireplace?";
                this.stateCur = GameState.WINDOW;
                break;
            case GameState.WINDOW:
                if(sInput.toLowerCase().match("investigate")){
                    sReply = "You step out into the cold rainy outdoors, you see a shadow in the corner. Do you investigate to see what it is or ignore it and go back to the fireplace?";
                    this.stateCur = GameState.BUSH;
                }else{
                    sReply ="You close the door and are turning to go back to the fireplace when BANG! The door swings open once again! Do you go investigate or go back to the fireplace?";
                    this.stateCur = GameState.FIREPLACE;
                }
                break;
            case GameState.FIREPLACE:
                if(sInput.toLowerCase().match("fireplace")){
                    sReply = "You continue to read your book by the cozy fire when you hear a tapping noise on the back window, do you ignore it or go check to see what's making the noise?";
                    this.stateCur = GameState.TAP;
                }else{
                    sReply = "You step out into the cold rainy outdoors, you see a shadow in the corner. Do you investigate to see what it is or ignore it and go back into the comfort of your warm cozy home?";
                    this.stateCur = GameState.BUSH;

                }
                break;
            case GameState.TAP:
                if(sInput.toLowerCase().match("check")){
                    sReply = "You put your book down once again, and go to the window. Nothing out of the ordinary. Do you open the door to investigate further or go back?";
                    this.stateCur = GameState.WINDOW;

                }else{
                    sReply = "You continue to read your book by the cozy fire when you hear a tapping noise on the back window, do you stay by the fireplace go check to see what's making the tap noise?";
                    this.stateCur = GameState.FIREPLACE;
    
                }
                break;
                case GameState.OUTSIDE:
                if(sInput.toLowerCase().match("investigate")){
                    sReply = "You open the door to see a shadowy figure in the corner, do you go investigate or go back to the fireplace?";
                    this.stateCur = GameState.WINDOW;

                }else{
                    sReply = "You continue to read your book by the cozy fire when you hear a tapping noise on the back window, do you ignore it or go check to see what's making the tap noise?";
                    this.stateCur = GameState.INVESTIGATE;
    
                }
                break;
                case GameState.INVESTIGATE:
                if(sInput.toLowerCase().match("check")){
                    sReply = "You put your book down once again, and go to the window. Nothing out of the ordinary. Do you open the door to investigate further or go back?";
                    this.stateCur = GameState.WINDOW;

                }else{
                    sReply = "You continue to read your book by the cozy fire when you hear a tapping noise on the back window once again, This time you go investigate (Type INVESTIGATE)";
                    this.stateCur = GameState.BUSH;
    
                }
                break;
                case GameState.BUSH:
                if(sInput.toLowerCase().match("investigate")){
                    sReply = "You go to the bush, and use your phone flashlight to take a better look. You look closer when suddenly...Meow! It's your pet cat Autumn rustling in the bushes. With a sigh of relief that it wasn't anything scaier, you pick Autumn up and go back to the fireplace where Autumn warms up by the fire while you pet her and conitune to read your book until you both fall asleep.";
                    this.stateCur = GameState.WELCOMING;

                }else{
                    sReply = "You continue to read your book by the cozy fire when you hear a tapping noise on the back window once again, This time you go investigate (Type INVESTIGATE)?";
                    this.stateCur = GameState.BUSH;
    
                }
                break;
        }
        return([sReply]);
    }
}